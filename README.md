# English Text Summarization

This is a Text Summarization Project
This project uses TF/IDF to score the sentences of a paragraph, then suggests multiple summaries.
And finally gives the best summary that is close to the main paragraph depending on the cosine similarity in semantic space.